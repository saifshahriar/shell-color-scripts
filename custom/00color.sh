#!/bin/sh
  
# ANSI Color -- use these variables to easily have different color
#    and format output. Make sure to output the reset sequence after 
#    colors (f = foreground, b = background), and use the 'off'
#    feature for anything you turn on.
#
# Author: 
# 
# Source: 
#
# 

initializeANSI() {
  esc=""

# foreground
  blackf="${esc}[30m"
  bluef="${esc}[34m"
  cyanf="${esc}[36m"
  greenf="${esc}[32m"
  purplef="${esc}[35m"
  redf="${esc}[31m"
  whitef="${esc}[37m"
  yellowf="${esc}[33m"

# bright
  blackfbright="${esc}[90m"
  bluefbright="${esc}[94m"
  cyanfbright="${esc}[96m"
  greenfbright="${esc}[92m"
  purplefbright="${esc}[95m"
  redfbright="${esc}[91m"
  whitefbright="${esc}[97m"
  yellowfbright="${esc}[93m"

# background
  blackb="${esc}[40m"
  blueb="${esc}[44m"
  cyanb="${esc}[46m"
  greenb="${esc}[42m"
  purpleb="${esc}[45m"
  redb="${esc}[41m"
  whiteb="${esc}[47m"
  yellowb="${esc}[43m"

# style
  boldon="${esc}[1m" 
  boldoff="${esc}[22m"
  invon="${esc}[7m"
  invoff="${esc}[27m"
  italicson="${esc}[3m"
  italicsoff="${esc}[23m"
  ulon="${esc}[4m"
  uloff="${esc}[24m"

  reset="${esc}[0m"
}

# note in this first use that switching colors doesn't require a reset
# first - the new color overrides the old one.

initializeANSI

# Create your ascii art here





# Example:
cat << EOF 
  
${boldon}${redfbright}        ■      ${boldon}${greenfbright}        ■      ${boldon}${yellowfbright}        ■     ${boldon}${bluefbright}         ■       ${boldon}${purplefbright}       ■      ${boldon}${cyanfbright}        ■   ${reset}
${boldon}${redfbright}       ■■■     ${boldon}${greenfbright}       ■■■     ${boldon}${yellowfbright}       ■■■    ${boldon}${bluefbright}        ■■■      ${boldon}${purplefbright}      ■■■     ${boldon}${cyanfbright}       ■■■  ${reset}
${boldon}${redfbright}      ■■■■■    ${boldon}${greenfbright}      ■■■■■    ${boldon}${yellowfbright}      ■■■■■   ${boldon}${bluefbright}       ■■■■■     ${boldon}${purplefbright}     ■■■■■    ${boldon}${cyanfbright}      ■■■■■ ${reset}
${redf}     ■(   )■   ${greenf}     ■(   )■   ${yellowf}     ■(   )■   ${bluef}     ■(   )■    ${purplef}    ■(   )■   ${cyanf}     ■(   )■   ${reset}
${redf}    ■■■■ ■■■■  ${greenf}    ■■■■ ■■■■  ${yellowf}    ■■■■ ■■■■  ${bluef}    ■■■■ ■■■■   ${purplef}   ■■■■ ■■■■  ${cyanf}    ■■■■ ■■■■  ${reset}
${redf}   ■■       ■■ ${greenf}   ■■       ■■ ${yellowf}   ■■       ■■ ${bluef}   ■■       ■■  ${purplef}  ■■       ■■ ${cyanf}   ■■       ■■ ${reset}
 
EOF
