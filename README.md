# Shell Color Scripts

![Screenshot of shell-color-scripts](https://gitlab.com/dwt1/dotfiles/raw/master/.screenshots/dotfiles12.png)

A collection of terminal color scripts I've accumulated over the years.
Included 52 beautiful terminal color scripts.

# Install
## Oneclick Install 
Only for bash, zsh & fish shell. For other shell, please use the Manual Installation method.
```bash
curl https://gitlab.com/saifshahriar/shell-color-scripts/-/raw/master/install.sh | sh
``` 
## Manual Install
- Copy and paste the following command to your terminal.
```bash
curl https://gitlab.com/saifshahriar/shell-color-scripts/-/raw/master/man-install.sh | sh
```
- Now add the following line to your shell config file.
```bash
find ~/.config/shell-color-scripts/colorscripts -type f | shuf -n 1 | sh 
```
- Restart your terminal and enjoy. ^_^

# Available colorscripts
```bash
ls ~/.config/shell-color-scripts/colorscripts
```

# Add my custom repo of colorscripts
```bash
chmod +x ~/.config/shell-color-scripts/custom/*; cp ~/.config/shell-color-scripts/custom/* ~/.config/shell-color-scripts/colorscripts
```

# List of my custom colorscripts
```bash
..
 ├── 00color.sh
 ├── bspwm
 ├── bspwm-suckless
 ├── dwm
 ├── st
 └── surf
```
also in github
