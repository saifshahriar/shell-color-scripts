#!/bin/bash

git clone https://gitlab.com/saifshahriar/shell-color-scripts.git
cp -r shell-color-scripts ~/.config
echo "find ~/.config/shell-color-scripts/colorscripts -type f | shuf -n 1 | sh" >> ~/.bashrc
echo "find ~/.config/shell-color-scripts/colorscripts -type f | shuf -n 1 | sh" >> ~/.zshrc
echo "find ~/.config/shell-color-scripts/colorscripts -type f | shuf -n 1 | sh" >> ~/.config/fish/config.fish
rm -rf shell-color-scripts
find ~/.config/shell-color-scripts/colorscripts -type f | shuf -n 1 > ~/.config/shell-color-scripts/temp.sh; sh ~/.config/shell-color-scripts/temp.sh
